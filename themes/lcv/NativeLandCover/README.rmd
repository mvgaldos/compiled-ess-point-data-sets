---
title: "Global compilation of native / natural vegetation land cover classes"
author: "Tom Hengl (tom.hengl@OpenGeoHub.org), Martin Jung (IAASA), Piero Visconti (IAASA)"
date: "6/20/2020"
output: 
  github_document:
    toc: true
bibliography: ../../../tex/refs.bib
csl: ../../../tex/apa.csl  
fig_caption: yes
link-citations: yes
twitter-handle: opengeohub
header-includes:
- \usepackage{caption}
---

[<img src="../../../tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

Part of: [Compiled ESS point data sets](https://gitlab.com/openlandmap/compiled-ess-point-data-sets)
Last update:  `r Sys.Date()`

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Read more about mapping natural/native land cover classes and species:

- [NatureMap.Earth](https://naturemap.earth/) project.
- Hengl, T., Jung, M., & Visconti, P., (2020). **Potential distribution of land cover classes (Potential Natural Vegetation) at 250 m spatial resolution (Version v0.1)** [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3631253,

## ![alt text](../../../tex/R_logo.svg.png "Packages in use") Specifications

```{r}
library(rgdal)
library(raster)
library(plyr)
library(fastSave)
library(googledrive)
library(openxlsx)
library(fastSave)
#load("training_points.RData")
source("../../../R/data_functions.R")
lc100m = raster("/mnt/DATA/Copernicus_vito/GLC100m/discrete-classification.vrt")
lc100m
```

- Metadata information: Fritz, S., See, L., Perger, C., McCallum, I., Schill, C., Schepaschenko, D., ... & Lesiv, M. (2017). [A global dataset of crowdsourced land cover and land use reference data](https://www.nature.com/articles/sdata201775). Scientific data, 4, 170075, [NatureMap explorer](https://explorer.naturemap.earth/map).
- Sample DB: NA

_Target variables:_

```{r}
col.names = c("source_db", "point_id", "map_code_pnv", "map_code_glc100m", "longitude_decimal_degrees", "latitude_decimal_degrees")
```

- `map_code_pnv`: global land cover classes based on the [GLC100m map](https://lcviewer.vito.be/about) (native vegetation only), 
- `map_code_glc100m`: global land cover classes based on the [GLC100m map](https://lcviewer.vito.be/about), 

```{r}
if(!exists("glc.tbl")){
  gs.url = as_id("https://docs.google.com/spreadsheets/d/10Y8PryG5TiPZTiArteRXE0iFdKUhNYhj38qb9bRnpVU/edit")
  xlsxFile = "/mnt/DATA/Geo-wiki/Global_forest_management_and_LC_mapping_tables.xlsx"
  if(!file.exists(xlsxFile)){ drive_download(gs.url, xlsxFile, overwrite = TRUE) }
  wb = openxlsx::getSheetNames(xlsxFile)
  wb
  esa.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "ESA_GLC_legend")
  #head(esa.tbl)
  gp.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "human_impact_on_forests_legend")
  lp.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "LandPKS_land_cover")
  gp_c.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "HIF_GLC_cross")
  lp_c.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "LandPKS_GLC_cross")
  bm.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "Biome_legend_original")
  wt.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "Tropical_Wetlands")
  #glc.tbl = openxlsx::read.xlsx(xlsxFile, sheet = "Pivot Table 1")
  glc.tbl = read.csv("/mnt/DATA/Geo-wiki/training_points/LC_label.csv")
}
str(glc.tbl)
```

## ![alt text](../../../tex/R_logo.svg.png "Data import") Data import

- Hengl T, Walsh MG, Sanderman J, Wheeler I, Harrison SP, Prentice IC. 2018. [Global mapping of potential natural vegetation: an assessment of machine learning algorithms for estimating land potential](https://doi.org/10.7717/peerj.5457). PeerJ 6:e5457 https://doi.org/10.7717/peerj.5457

```{r}
if(!exists("bm.ll")){
  biome_f = readRDS("/mnt/DATA/Geo-wiki/training_points/biome_f.rds")
  biome.df = as.data.frame(biome_f[c("Site.Name","V1")])
  biome.df = plyr::rename(biome.df, replace = c("V1"="Original_biome_classification"))
  ## 8797 points
  bm.pnts = plyr::join(biome.df[,c("Site.Name","Longitude","Latitude","Original_biome_classification")], bm.tbl[,c("Original_biome_classification","map_code_pnv")])
  bm.pnts$pv_mapping = 1
  bm.pnts = bm.pnts[!is.na(bm.pnts$map_code_pnv),]
  coordinates(bm.pnts) = ~ Longitude+Latitude
  proj4string(bm.pnts) = CRS("+init=epsg:4326")
  ov.bm = raster::extract(lc100m, bm.pnts)
  bm.pnts$lc100m = ov.bm
  bm.pnts$source_db = "Biome6000"
  bm.ll = as.data.frame(bm.pnts)
}
summary(as.factor(bm.pnts$map_code_pnv))
```

- Fritz, S., See, L., Perger, C., McCallum, I., Schill, C., Schepaschenko, D., ... & Lesiv, M. (2017). [A global dataset of crowdsourced land cover and land use reference data](https://www.nature.com/articles/sdata201775). Scientific data, 4, 170075.
- Marcel Buchhorn, Bruno Smets, Luc Bertels, Myroslava Lesiv, Nandin-Erdene Tsendbazar, Martin Herold, & Steffen Fritz. (2019). Copernicus Global Land Service: Land Cover 100m: epoch 2015: Globe (Version V2.0.2) [Data set]. Zenodo. http://doi.org/10.5281/zenodo.3243509

```{r}
if(!exists("gg.ll")){
  gp = read.csv("/mnt/DATA/Geo-wiki/training_points/human_impact_on_forests.csv")
  gp = plyr::rename(gp, replace=c("name"="HIF_name"))
  ## 42,965
  gp.pnts = plyr::join(gp[,c("sample_id","x","y","HIF_name")], gp.tbl[,c("HIF_name","pv_mapping")])
  gp.pnts = gp.pnts[gp.pnts$pv_mapping==1 & !is.na(gp.pnts$x),]
  #str(gp.pnts)
  coordinates(gp.pnts) = ~ x+y
  proj4string(gp.pnts) = CRS("+init=epsg:4326")
  ov.gp = raster::extract(lc100m, gp.pnts)
  ## 26,655
  gp.pnts$lc100m = ov.gp
  gp.pnts$HIF_name_lc = paste(gp.pnts$HIF_name, gp.pnts$lc100m, sep="_")
  x = data.frame(t(summary(as.factor(gp.pnts$HIF_name_lc))))
  #write.csv(x, "./tmp/HIF_name_lc.csv")
  ## merge:
  gp_c.tbl$HIF_name_lc = gsub("X", "", paste0(gp_c.tbl$HIF_name_lc))
  gg.ll = plyr::join(as.data.frame(gp.pnts), gp_c.tbl[,c("HIF_name_lc","map_code_pnv")])
  gg.ll = gg.ll[!is.na(gg.ll$map_code_pnv),]
  gg.ll$source_db = "Geo-wiki_HIF"
}
summary(as.factor(gg.ll$map_code_pnv))
```

- Herrick, J. E., Urama, K. C., Karl, J. W., Boos, J., Johnson, M. V. V., Shepherd, K. D., ... & Kosnik, C. (2013). [The Global Land-Potential Knowledge System (LandPKS): Supporting Evidence-based, Site-specific Land Use and Management through Cloud Computing, Mobile Applications, and Crowdsourcing](https://doi.org/10.2489/jswc.68.1.5A). Journal of Soil and Water Conservation, 68(1), 5A-12A. Data download URL: https://landpotential.org/data-portal/ 

NOTE: LandPKS field observations but many in managed areas / different legends.

```{r}
if(!exists("lp.ll")){
  lp = readRDS("/mnt/DATA/Geo-wiki/training_points/LandPKS_points_Aug_2019.rds")
  #names(lp)
  str(summary(lp$land_cover))
  lp$land_cover_class = make.names(lp$land_cover, unique = FALSE, allow_ = TRUE)
  lp[1,c("id","land_cover","land_cover_class")]
  lp.pnts = plyr::join(lp[,c("id","land_cover_class","longitude.f","latitude.f")], lp.tbl[,c("land_cover_class","pv_mapping","map_code_pnv")])
  coordinates(lp.pnts) = ~ longitude.f+latitude.f
  proj4string(lp.pnts) = CRS("+init=epsg:4326")
  ov.lp = raster::extract(lc100m, lp.pnts)
  lp.pnts$lc100m = ov.lp
  lp.pnts$land_cover_lc = paste(lp.pnts$land_cover_class, lp.pnts$lc100m, sep="_")
  x = data.frame(t(summary(as.factor(lp.pnts$land_cover_lc))))
  #write.csv(x, "./tmp/land_cover_lc.csv")
  lp.ll = plyr::join(as.data.frame(lp.pnts)[c("id","longitude.f","latitude.f","land_cover_lc","lc100m")], lp_c.tbl[,c("land_cover_lc","map_code_pnv")])
  lp.ll = lp.ll[!is.na(lp.ll$map_code_pnv),]
  lp.ll$source_db = "LandPKS_land_cover"
  dim(lp.ll)
}
summary(as.factor(lp.ll$map_code_pnv))
```

- Simulated points using the 100 m resolution Land cover map from https://lcviewer.vito.be/

```{r}
if(!exists("sim.ll")){
  ## NOTE: overlay with GLC100 and only use highly certain points with prob>90
  sim = readOGR("/mnt/DATA/Geo-wiki/training_points/intact_rpoints.gpkg")
  lc.p = list.files("/mnt/DATA/Landsat/", glob2rx("*coverfraction-layer.tif$"), full.names = TRUE)
  ov.sim = parallel::mclapply(lc.p, function(i){raster::extract(raster(i), sim)}, mc.cores = length(lc.p))
  #str(ov.sim)
  names(ov.sim) = basename(lc.p)
  ov0.sim = data.frame(raster::extract(lc100m, sim))
  names(ov0.sim) = basename(lc100m@file@name)
  sim.ll = do.call(cbind, list(ov.sim, ov0.sim, as.data.frame(sim)))
  ## highest prob:
  sim.ll[, "max"] <- apply(sim.ll[,grep("-coverfraction-layer", names(sim.ll))], 1, max)
  #str(sim.ll)
  #head(sim.ll)
  summary(sim.ll$max>90)
  summary(as.factor(sim.ll$`discrete-classification.vrt`))
  sel.sim = (sim.ll$`discrete-classification.vrt` %in% esa.tbl$map_code_pnv & sim.ll$max > 90) | sim.ll$`discrete-classification.vrt` %in% c(111,113,112,114,115,121,123,122,124,125)
  summary(sel.sim)
  ## 12,136
  sim.ll = sim.ll[which(sel.sim),]
  sim.ll$source_db = "Simulated_IFL_WPA"
  #str(sim.ll)
}
summary(as.factor(sim.ll$`discrete-classification.vrt`))
```

- Gell, P. A., Finlayson, C. M., & Davidson, N. C. (2016). Understanding change in the ecological character of Ramsar wetlands: perspectives from a deeper time–synthesis. Marine and Freshwater Research, 67(6), 869-879. Data download URL: https://rsis.ramsar.org/

```{r}
if(!exists("ram.ll")){
  ## exclude "Human-made wetlands"
  ## ALL class 90
  ram.pol = readOGR("/mnt/DATA/RAMSAR/ramsar_polygons/features_publishedPolygon.shp")
  #str(ram.pol@data)
  #summary(as.factor(ram.pol$ramsarid))
  ov.ram = over(sim, ram.pol)
  ov.ram = cbind(ov.ram, sim@coords)
  ram.ll = ov.ram[!is.na(ov.ram$ramsarid),c("v_idris","coords.x1","coords.x2")]
  ram.ll$map_code_pnv = 90
  ram.ll$map_code_glc100m = NA
  ram.ll$source_db = "RAMSAR"
}
summary(as.factor(ram.ll$map_code_pnv))
```

- Schepaschenko, D., See, L., Lesiv, M., McCallum, I., Fritz, S., Salk, C., ... & Kovalevskyi, S. (2015). [Development of a global hybrid forest mask through the synergy of remote sensing, crowdsourcing and FAO statistics](https://doi.org/10.1016/j.rse.2015.02.011). Remote Sensing of Environment, 162, 208-220.

```{r}
if(!exists("sim.LC")){
  ## Extra simulated points per continent: EU, USA, Canada, Australia, World
  system('gdal_translate /data/Geo-wiki/archive/for2000_bg/for2000_bg.tif /mnt/DATA/protectedplanet/for2000_bg.tif -ot \"Byte\" -a_nodata 128 -co \"COMPRESS=DEFLATE\"')
  Ni = 100000
  mask.lst = c("ifl_2013.tif", "ifl_2000.tif", "WDPA_Dec2017-shapefile-polygons.tif", "for2000_bg.tif")
  ## sample points randomly using masks above (takes >20 mins)
  rpnt.lst = parallel::mclapply(mask.lst, function(i){ raster::sampleRandom(raster(paste0("/mnt/DATA/protectedplanet/", i)), size=Ni, sp=TRUE) }, mc.cores=4)
  rpnt.df.lst = do.call(rbind, lapply(rpnt.lst, function(i) as.data.frame(i)[,c("x","y")]))
  names(rpnt.df.lst) = c("Lon","Lat")
  rpnt.df.lst$ID = paste0(c(make.unique(rep("IFL_2013", nrow(rpnt.lst[[1]]))), make.unique(rep("IFL_2000", nrow(rpnt.lst[[2]]))), make.unique(rep("WDPA_Dec2017", nrow(rpnt.lst[[3]]))), make.unique(rep("ForestMask2000", nrow(rpnt.lst[[4]])))))
  str(rpnt.df.lst)
  coordinates(rpnt.df.lst) = ~ Lon + Lat
  proj4string(rpnt.df.lst) = "+init=epsg:4326"
  #plot(rpnt.df.lst)
  unlink("/mnt/DATA/Geo-wiki/training_points/intact_rpoints50k.gpkg")
  writeOGR(rpnt.df.lst, "/data/Geo-wiki/training_points/intact_rpoints50k.gpkg", "intact_rpoints50k", "GPKG")
  saveRDS(rpnt.df.lst, "/data/Geo-wiki/training_points/intact_rpoints50k.rds")
  #rpnt.df.lst = readRDS("/data/Geo-wiki/training_points/intact_rpoints50k.rds")
  ## LC 5 continents ----
  cont.list = c("/mnt/DATA/Geo-wiki/Continents/CLC2018_CLC2018_V2018_20.tif",
                "/mnt/DATA/Geo-wiki/Continents/CAN_NALCMS_LC_30m_LAEA_mmu12_urb05_CAL.tif",
                "/mnt/DATA/Geo-wiki/Continents/DLCDv1_Class.tif",
                "/mnt/DATA/Geo-wiki/Continents/GLC2010_100m.tif",
                "/mnt/DATA/Geo-wiki/Continents/NLCD_2016_Land_Cover_L48_20190424.img")
  con.leg = openxlsx::read.xlsx(xlsxFile, sheet = "Continents_legends")
  ## function to extract only classes of interest
  extrac.f <- function(x, rpnt.df.lst, con.leg){
    R <- raster::raster(x)
    ov <- raster::extract(R, spTransform(rpnt.df.lst, proj4string(R)), sp=TRUE)
    ov.f <- ov[!is.na(ov@data[,names(R)[1]]),]
    ov.f <- spTransform(ov.f, CRS("+init=epsg:4326"))
    ov.f$map_code_pnv = plyr::join(data.frame(Value=ov.f@data[,names(R)[1]]), con.leg[which(con.leg$Filename==names(R)[1]),])$map_code_pnv
    ov.f <- ov.f[!is.na(ov.f$map_code_pnv),]
    return(ov.f)
  }
  ov.rnd = parallel::mclapply(cont.list, function(i){extrac.f(i, rpnt.df.lst, con.leg)}, mc.cores = length(cont.list))
  sim.LC = plyr::rbind.fill(lapply(ov.rnd, function(i){as.data.frame(i[c("ID","map_code_pnv")])}))
  sim.LC$source_db = "Land_Cover_maps"
  sim.LC$map_code_glc100m = NA
  #str(sim.LC)
  #plot(sim.LC[,c("Lon","Lat")])
}
summary(as.factor(sim.LC$map_code_pnv))
```

- Sanderman, J., Hengl, T., Fiske, G., Solvik, K., Adame, M. F., Benson, L., ... & Duncan, C. (2018). [A global map of mangrove forest soil carbon at 30 m spatial resolution](https://doi.org/10.1088/1748-9326/aabe1c). Environmental Research Letters, 13(5), 055002.

```{r}
if(!exists("mng")){
  mng = readOGR("/mnt/projects/mangroves/soildata/mangroves_SOC_points.gpkg")
  mng = as.data.frame(mng)
  mng = mng[!duplicated(mng$LOC_ID),]
  mng$map_code_pnv = 127
  mng$map_code_glc100m = NA
  dim(mng)
}
summary(as.factor(mng$map_code_pnv))
```

- Chen, B., Xu, B., Zhu, Z., Yuan, C., Suen, H. P., Guo, J., ... & Yu, C. (2019). [Stable classification with limited sample: Transferring a 30-m resolution sample set collected in 2015 to mapping 10-m resolution global land cover in 2017](https://doi.org/10.1016/j.scib.2019.03.002). Sci Bull, 64, 370-373. FROM-GLC Global validation sample set (v1). Data download URL: http://data.ess.tsinghua.edu.cn/

```{r}
if(!exists("glc.ll")){
  glc.val = read.csv("/mnt/DATA/Geo-wiki/training_points/GlobalLandCoverValidationSampleSet_v1.csv")
  str(glc.val)
  ## 38,664 points
  ## subset to protected planet
  ov.glc = parallel::mclapply(mask.lst, function(i){ raster::extract(raster(paste0("/mnt/DATA/protectedplanet/", i)), glc.val[,c("Lon","Lat")]) }, mc.cores=3)
  sel.glc = !is.na(ov.glc[[1]]) | !is.na(ov.glc[[2]]) | !is.na(ov.glc[[3]])
  summary(sel.glc)
  glc.ll = plyr::join(glc.val[sel.glc,c("ID","Lon","Lat","LC.Label")], glc.tbl[,c("LC.Label","map_code_pnv","pv_mapping")])
  glc.ll = glc.ll[!is.na(glc.ll$map_code_pnv),]
  dim(glc.ll)
  glc.ll$source_db = "FROM_GLC"
  glc.ll$map_code_glc100m = NA
}
summary(as.factor(glc.ll$map_code_pnv))
```

- IIASA staff, expert-based points

```{r}
if(!exists("geof.ll")){
  geof = readOGR("/mnt/DATA/Geo-wiki/training_points/GeowikiFeedback.gpkg")
  str(geof@data)
  geof.ll = spsample(geof["map_code_pnv"], n = 1000, type = "random")
  ov.geof = over(geof.ll, geof["map_code_pnv"])
  summary(as.factor(ov.geof$map_code_pnv))
  geof.ll = cbind(as.data.frame(geof.ll), ov.geof)
  geof.ll = geof.ll[!is.na(geof.ll$map_code_pnv),]
  geof.ll$map_code_glc100m = NA
  geof.ll$source_db = "GeoFeedback"
  geof.ll$ID = paste0("GF", 1:nrow(geof.ll))
}
summary(as.factor(geof.ll$map_code_pnv))
```

- Brasilian native vegetation provided by: Eduardo Lacerda <e.lacerda@iis-rio.org>

```{r}
if(!exists("bra.ll")){
  pol.lst = paste0("/data/Geo-wiki/training_points/pnv_brazil/", c("amazonian_savanna.shp", "cerrado.shp", "megafans.shp", "pampa.shp", "pantanal.shp"))
  #cls.lst = c(20, 20, 90, 20, 90)
  ov.bra = list(NULL)
  for(i in 1:length(pol.lst)){
    x = readOGR(pol.lst[i])
    x = spTransform(x, proj4string(sim))
    if(length(grep("mapcodepnv", names(x)))==0){ x$mapcodepnv <- 20 }
    y = over(sim, x["mapcodepnv"])
    ov.bra[[i]] = cbind(y, sim@coords)
    ov.bra[[i]] = ov.bra[[i]][!is.na(ov.bra[[i]]$mapcodepnv),]
  }
  bra.ll = do.call(rbind, ov.bra)
  str(bra.ll)
  bra.ll$source_db = "Brasil_iis_rio"
  bra.ll$map_code_glc100m = NA
  bra.ll$ID = paste0("BR", 1:nrow(bra.ll))
}
summary(as.factor(bra.ll$mapcodepnv))
```

- Various literature sources prepared by: Martin Jung <jung@iiasa.ac.at>

```{r}
if(!exists("lit.lst")){
  lit.lst = lapply(list.files("/mnt/DATA/Geo-wiki/training_points/MoreTrainingdata", pattern = ".csv", full.names = TRUE), read.csv)
  lit.lst = dplyr::bind_rows(lit.lst)
  str(lit.lst)
  lit.lst$ID = make.unique(make.names(lit.lst$source_db))
  lit.lst$map_code_glc100m = NA
}
```

- Kindt, R., Lillesø, J. P. B., van Breugel, P., Bingham, M., Demissew, S., Dudley, C., ... & Minani, V. (2014). Potential Natural Vegetation of Eastern Africa (Ethiopia, Kenya, Malawi, Rwanda, Tanzania, Uganda and Zambia). Department of Geoscience and Natural Resource Management, University of Copenhagen. http://vegetationmap4africa.org

```{r}
if(!exists("vec.ll")){
  vec.ll = read.csv("/mnt/DATA/Geo-wiki/training_points/VECEA_sampledPoints.csv")
  str(vec.ll)
  #plot(vec.ll[,c("x1","x2")])
  vec.ll$ID = make.unique(make.names(vec.ll$source_db))
  vec.ll$map_code_glc100m = NA
}
```

- Richardson, A.D., Hufkens, K., Milliman, T., Aubrecht, D.M., Chen, M., Gray, J.M., Johnston, M.R., Keenan, T.F., Klosterman, S.T., Kosmala, M., Melaas, E.K., Friedl, M.A., Frolking, S.  2018. [Tracking vegetation phenology across diverse North American biomes using PhenoCam imagery](https://doi.org/10.1038/sdata.2018.28). Scientific Data 180028. DOI: https://doi.org/10.1038/sdata.2018.28. Data download URL:  https://cran.r-project.org/web/packages/phenocamapi/vignettes/getting_started_phenocam_api.html

```{r}
if(!exists("rois.ll")){
  rois.ll = readRDS("/mnt/DATA/Geo-wiki/training_points/phenocamr_rois.rds")
  rois.ll$map_code_glc100m = NA
  rois.ll$source_db = "Phenocam"
}
```

- Global wetlands https://www.cifor.org/global-wetlands/

Note: This is very much a model, not a point dataset. Swamp/bog certainly includes peat-swamp forests.

```{r}
if(!exists("wet.ll")){
  wetlandV2 = raster("/mnt/DATA/Geo-wiki/Continents/TROP-SUBTROP_WetlandV2_2016_CIFOR.tif")
  NAvalue(wetlandV2) = 0
  ## takes ca 10mins:
  wet.val = sampleRandom(wetlandV2, size = 60000, sp=TRUE)
  #plot(wet.val)
  wet.val$ID = paste0("IDrnd", 1:nrow(wet.val))
  wet.ll = plyr::join(as.data.frame(wet.val)[,c("ID","x","y","TROP.SUBTROP_WetlandV2_2016_CIFOR")], wt.tbl[,c("TROP.SUBTROP_WetlandV2_2016_CIFOR","map_code_pnv")])
  wet.ll = wet.ll[!is.na(wet.ll$map_code_pnv),]
  wet.ll$source_db = "Global_Wetlands"
  wet.ll$map_code_glc100m = NA
  dim(wet.ll)
}
summary(as.factor(wet.ll$map_code_pnv))
```

## ![alt text](../../../tex/R_logo.svg.png "Bind everything") Bind everything

```{r}
library(dplyr)
tot_pnts = dplyr::bind_rows(lapply(list(
  gg.ll[,c("source_db","sample_id","map_code_pnv","lc100m","x","y")], 
  lp.ll[,c("source_db","id","map_code_pnv","lc100m","longitude.f", "latitude.f")], 
  sim.ll[,c("source_db","ID","discrete-classification.vrt","discrete-classification.vrt","coords.x1","coords.x2")], 
  sim.LC[,c("source_db","ID","map_code_pnv","map_code_glc100m","Lon","Lat")],
  glc.ll[,c("source_db","ID","map_code_pnv","map_code_glc100m","Lon","Lat")],
  bm.ll[,c("source_db","Site.Name","map_code_pnv","lc100m","Longitude","Latitude")],
  #ram.ll[,c("source_db","v_idris","map_code_pnv","map_code_glc100m","coords.x1","coords.x2")],
  bra.ll[,c("source_db","ID","mapcodepnv","map_code_glc100m","coords.x1","coords.x2")],
  geof.ll[,c("source_db","ID","map_code_pnv","map_code_glc100m","x","y")],
  mng[,c("SOURCEDB","SOURCEID","map_code_pnv","map_code_glc100m","coords.x1","coords.x2")],
  lit.lst[,c("source_db","ID","map_code_pnv","map_code_glc100m","Longitude", "Latitude")],
  vec.ll[,c("source_db","ID","map_code_pnv","map_code_glc100m","x1", "x2")],
  #wet.ll[,c("source_db","ID","map_code_pnv","map_code_glc100m","x", "y")],
  rois.ll[,c("source_db","roi_id_number","map_code_pnv","map_code_glc100m","lon", "lat")]
  ), 
  function(i){ mutate_all(setNames(i, col.names), as.character) }))
tot_pnts$longitude_decimal_degrees = as.numeric(tot_pnts$longitude_decimal_degrees)
tot_pnts$latitude_decimal_degrees = as.numeric(tot_pnts$latitude_decimal_degrees)
str(tot_pnts)
## remove some points based expert review (Martin Jung):
pnts2rm = readOGR("/mnt/DATA/Geo-wiki/training_points/PointsToRemove.gpkg")
sel.pnts2rm = which(tot_pnts$longitude_decimal_degrees %in% pnts2rm@coords[,1] & tot_pnts$latitude_decimal_degrees %in% pnts2rm@coords[,2])
tot_pnts = tot_pnts[-sel.pnts2rm,]
```

Summary statistics for all points:

```{r}
sumstat = t(data.frame(as.list(summary(as.factor(tot_pnts$source_db)))))
write.table(sumstat, file = "pnv_classes_sumstats.csv", sep = ",", row.names = TRUE)
sel.tot = tot_pnts$map_code_pnv %in% unique(esa.tbl$map_code_pnv)
summary(sel.tot)
tot_pnts = tot_pnts[sel.tot,]
coordinates(tot_pnts) <- ~ longitude_decimal_degrees+latitude_decimal_degrees
proj4string(tot_pnts) = "+proj=longlat +datum=WGS84"
length(tot_pnts)
```

```{r}
#saveRDS(tot_pnts, "glc100m_natural_vegetation.pnts.rds")
#file.copy("glc100m_natural_vegetation.pnts.rds", "/data/LandGIS/training_points/vegetation/glc100m_natural_vegetation.pnts.rds", overwrite=TRUE)
#unlink("glc100m_natural_vegetation.pnts.gpkg")
#writeOGR(tot_pnts, "glc100m_natural_vegetation.pnts.gpkg", "glc100m_natural_vegetation.pnts", "GPKG")
#file.copy("glc100m_natural_vegetation.pnts.gpkg", "/data/LandGIS/training_points/vegetation/glc100m_natural_vegetation.pnts.gpkg", overwrite=TRUE)
save.image.pigz("training_points.RData")
```


```{r sol_chem.pnts_sites, echo=FALSE, fig.cap="Soil hydraulic and physical soil properties.", out.width="100%"}
knitr::include_graphics("../../../img/lcv_nat.landcover.pnts_sites.png")
```

World map of distribution of points:

```{r}
if(!file.exists("../../../img/lcv_nat.landcover.pnts_sites.png")){
  library(sf)
  tot_pnts_sf <- st_as_sf(tot_pnts[1])
  plot_gh(tot_pnts_sf, out.pdf="../../../img/lcv_nat.landcover.pnts_sites.pdf")
  system("pdftoppm ../../../img/lcv_nat.landcover.pnts_sites.pdf ./img/lcv_nat.landcover.pnts_sites -png -f 1 -singlefile")
  system("convert -crop 1280x575+36+114 ../../../img/lcv_nat.landcover.pnts_sites.png ../../../img/lcv_nat.landcover.pnts_sites.png")
}
```

## ![alt text](../../../tex/R_logo.svg.png "Overlay www.OpenLandMap.org layers") Overlay www.OpenLandMap.org layers

Load the tiling system (1 degree grid representing global land mask) and run spatial overlay in parallel:

```{r}
if(!exists("rm.lc")){
  tile.pol = readOGR("../../../tiles/global_tiling_100km_grid.gpkg")
  #length(tile.pol)
  ov.lc <- extract.tiled(obj=tot_pnts, tile.pol=tile.pol, path="/data/tt/LandGIS/grid250m", ID="ID", cpus=64)
  summary(!is.na(ov.lc$map_code_pnv))
  ov.lc = ov.lc[!is.na(ov.lc$map_code_pnv),]
  ov.lc$map_code_pnv.f = as.factor(make.names(ov.lc$map_code_pnv))
  summary(ov.lc$map_code_pnv.f)
  ## Valid predictors:
  pr.vars = make.names(unlist(sapply(c("sm2rain","monthly.temp_worldclim.chelsa","bioclim.var_chelsa","irradiation_solar.atlas", "usgs.ecotapestry", "floodmap.500y", "water.table.depth_deltares", "snow.prob_esacci", "water.vapor_nasa.eo", "wind.speed_terraclimate", "merit.dem_m", "merit.hydro_m", "cloud.fraction_earthenv", "water.occurance_jrc", "wetlands.cw_upmc", "pb2002"), function(i){names(ov.lc)[grep(i, names(ov.lc))]})))
  str(pr.vars)
  ## Final classification matrix:
  rm.lc = ov.lc[complete.cases(ov.lc[,pr.vars]),]
}
dim(rm.lc)
```

Save final Analysis Ready Data:

```{r}
saveRDS.gz(as.data.frame(tot_pnts), "../../../out/rds/lcv_nat.landcover.pnts_sites.rds")
library(farff)
writeARFF(as.data.frame(tot_pnts), "../../../out/arff/lcv_nat.landcover.pnts_sites.arff", overwrite = TRUE)
saveRDS.gz(rm.lc, "../../../out/rds/lcv_nat.landcover.pnts_sites_cm.rds")
```
